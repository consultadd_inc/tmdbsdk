===============================
tmdbsdk
===============================



.. image:: https://pyup.io/repos/github/0loc/tmdbsdk/shield.svg
     :target: https://pyup.io/repos/github/0loc/tmdbsdk/
     :alt: Updates


A python client for tmdb APIs



Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

