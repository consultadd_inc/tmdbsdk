import os

# try:
# TIXDO_ADMIN_EMAIL = os.environ['TIXDO_ADMIN_EMAIL']
# TIXDO_ADMIN_PASSWORD = os.environ['TIXDO_ADMIN_PASSWORD']
# TIXDO_BASE_URL = os.environ['TIXDO_BASE_URL']
# TIXDO_BASE_PROTOCOL = os.environ['TIXDO_BASE_PROTOCOL']
try:
    TMDB_AUTH_TOKEN = os.environ['TMDB_AUTH_TOKEN']
except KeyError:
    TMDB_AUTH_TOKEN = "dummy"
# except KeyError:
    # TIXDO_BASE_URL="tdmainapp"
    # TIXDO_BASE_PROTOCOL="http"
    # TIXDO_ADMIN_EMAIL="labs@tixdo.com"
    # TIXDO_ADMIN_PASSWORD="1188"
    #
    # TIXDO_BASE_URL="localhost:8000"
    # TIXDO_BASE_PROTOCOL="http"
    # TIXDO_ADMIN_EMAIL="admin@gmail.com"
    # TIXDO_ADMIN_PASSWORD="admin"
    # TIXDO_BASE_URL="54.169.241.123"
    # TIXDO_BASE_PROTOCOL="http"
    # TIXDO_ADMIN_EMAIL="labs@tixdo.com"
    # TIXDO_ADMIN_PASSWORD="Tixdo@2016"


service_definitions = {

    "TmdbService": {
        "resources": {
            "get_movies": {
                "endpoint": "/movies/released_upcoming_movie",
                "required_params": [],
                "optional_params": [],
            },
            "get_released_movies": {
                "endpoint": "/movies/released_movie",
                "required_params": [],
                "optional_params": [],
            },
            "get_upcoming_movies": {
                "endpoint": "/movies/upcoming_movie",
                "required_params": [],
                "optional_params": [],
            },
            "releasing_today": {
                "endpoint": "/movies/releasing_today",
                "required_params": [],
                "optional_params": [],
            },
            "get_show_by_movie": {
                "endpoint": "/movies/shows/",
                "required_params": ['movie_slug'],
                "optional_params": [],
            },
            "get_theatres": {
                "endpoint": "/theatre/get_all_theatre",
                "required_params": [],
                "optional_params": [],
            },
            "theatres_by_remote": {
                "endpoint": "/theatre/by_remote_name/",
                "required_params": ['remote'],
                "optional_params": [],
            },
            "theatres_by_city": {
                "endpoint": "/theatre/by_city/",
                "required_params": ['city'],
                "optional_params": [],
            },
            "get_show_by_theatre": {
                "endpoint": "/theatre/shows/",
                "required_params": ['theatre_slug'],
                "optional_params": [],
            },
            "get_shows": {
                "endpoint": "/show/all_shows",
                "required_params": [],
                "optional_params": [],
            },
            "get_shows_by_remote": {
                "endpoint": "/show/by_remote_name/",
                "required_params": ['remote'],
                "optional_params": [],
            },
            "get_city": {
                "endpoint": "/city/all_city/",
                "required_params": [],
                "optional_params": [],
            },
            "get_show_by_city": {
                "endpoint": "/city/shows/",
                "required_params": ['city'],
                "optional_params": [],
            },
            "get_region": {
                "endpoint": "/static-region/all_region/",
                "required_params": [],
                "optional_params": [],
            },
            "get_sub_movie_by_raw_movie": {
                "endpoint": "/sub_movie/get_sub_movie/",
                "required_params": ['raw_movie_id'],
                "optional_params": [],
            },
            "get_show_by_movie_and_sub_movie": {
                "endpoint": "/movies/shows_by_sub_movie/",
                "required_params": ['raw_data'],
                "optional_params": [],
            },
            "get_screens_by_theatre": {
                "endpoint": "/theatre/get_screens_by_theatre/",
                "required_params": ['theatre_id'],
                "optional_params": [],
            },
        }
    }
}
