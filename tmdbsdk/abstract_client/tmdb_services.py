# Core Python imports
import logging
import json

from tmdbsdk.abstract_client.base_client import ServiceBase

from tmdbsdk import settings
from tmdbsdk.abstract_client.exceptions import PermissionDenied

logger = logging.getLogger(__name__)


class CustomError(Exception):
    def __init__(self, arg):
        # Set some exception infomation
        self.msg = arg


class TmdbService(ServiceBase):
    def __init__(self, base_url):
        # self.base_url = base_url
        # self.url = self.base_url["url"]
        # self.protocol = self.base_url["protocol"]
        # login_response = self._is_logged_in(super(TmdbService, self))
        # print("login_response -----> ",login_response)
        # if login_response[1]:
        #     self.is_logged_in = login_response[1]
        #     self.token = login_response[0]
        super(TmdbService, self).__init__('TmdbService', base_url)
        # else:
        #     raise PermissionDenied

    # @staticmethod
    #
    # def _is_logged_in(self, service):
    #     print("Hello")
    #     email = self.base_url['email']
    #     password = self.base_url['password']
    #     response = service.authenticate(email=email, password=password)
    #     is_logged_in, token = service.login(response)
    #     if not is_logged_in:
    #         logger.error("can not log in using given credentials: email: (%s), password: (%s)", email, password)
    #     return token, is_logged_in

    # Movie APIs

    def get_all_movies(self):
        try:
            data = self.create("get_movies").json()
            return data
        except ValueError as e:
            logging.error(e)
            return e

    def get_released_movies(self):
        try:
            data = self.create("get_released_movies").json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    def get_upcoming_movies(self):
        try:
            data = self.create("get_upcoming_movies").json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    def releasing_today(self):
        try:
            data = self.create("releasing_today").json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    def get_show_by_movie(self, movie_slug):
        try:
            data = self.create("get_show_by_movie", data=movie_slug).json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    def get_show_by_movie_and_sub_movie(self, raw_data):
        try:
            data = self.create("get_show_by_movie_and_sub_movie", data=raw_data).json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    # Theatre APIs

    def get_all_theatres(self):
        try:
            data = self.create("get_theatres").json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    def by_remote_name(self, remote):
        try:
            data = self.create("theatres_by_remote", data=remote).json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    def theatres_by_city(self, city):
        try:
            data = self.create("theatres_by_city", data=city).json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    def get_show_by_theatre(self, theatre_slug):
        try:
            data = self.create("get_show_by_theatre", data=theatre_slug).json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    # Show APIs

    def get_all_shows(self):
        try:
            data = self.create("get_shows").json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    def get_shows_by_remote(self, remote):
        try:
            data = self.create("get_shows_by_remote", data=remote).json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    # City APIs.

    def get_all_city(self):
        try:
            data = self.create("get_city").json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    def get_show_by_city(self, city):
        try:
            data = self.create("get_show_by_city", data=city).json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    # Static region APIs.

    def get_all_region(self):
        try:
            data = self.create("get_region").json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    # sub movie API.
    def get_sub_movie_by_raw_movie(self, raw_movie_id):
        try:
            data = self.create("get_sub_movie_by_raw_movie", data=raw_movie_id).json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e

    # screen API.
    def get_screens_by_theatre(self, theatre_id):
        try:
            data = self.create("get_screens_by_theatre", data=theatre_id).json()
            return data
        except ValueError as e:
            logging.debug(e)
            return e


