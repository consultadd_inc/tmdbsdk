class PermissionDenied(Exception):
    """The user did not have permission to do that"""
    pass